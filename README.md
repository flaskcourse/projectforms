# ProjectForms


![Project Forms preview](image140.png){width=50%} 


## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info

This is a training project in the Flask learning course. The project realizes the simple walker game in a browser.  We use package Flask_wtf and inherit from its class FlaskForm to create two forms: (i) on the start page to select field dimensions and on the game page to move the around. In total there are 6 (six) fields : heigth, width, direction, number of stapes and two submit fields. In three fields wich require number inputs there are correspondung validators for the type of input and number range.  
Also, flash messages are used to inform about the walls and about the and of game.

Styles are impoted from style.css static file. Templates are inherited from the base.html template. 

## Technologies
Project is created with:
* Windows 10 Enterprise
* Python 3.9.13 
* Flask==2.3.2
* Jinja2==3.1.2
It was tested in a browser 
* Microsoft Edge Version 114.0.1823.43 (Official build) (64-bit)

## Setup
Clone this repo to your desktop or download zip and unpack

## Usage
After you clone this repo to your desktop,   run pip install -r requirements.txt
to install all the dependencies.

Once the dependencies are installed, you can run app.py file to start the application. You will then be able to access it at localhost:5000
