from . import app
from flask import Flask, flash, render_template, url_for
from .game_walk import GameWalk
from config import Config
from werkzeug.utils import redirect

from .project.forms import MessageForm, StepForm
import os

@app.route('/', methods=['GET', 'POST'])
def index():
    height = 25
    width = 25

    form = MessageForm()
    if form.validate_on_submit():
        height = form.height.data
        width = form.width.data

#        print('\n Data received. Redirection ..')

        GameWalk(height, width)
        print(f"param {height}, {width}")

        return redirect(url_for('live_box', height=height, width=width))

    return render_template("index.html", form=form, width=width, heigth=height)

@app.route('/live_box/<height>/<width>', methods=['GET', 'POST'])
def live_box(height, width):
    game = GameWalk()
    form = StepForm()

    if form.validate_on_submit():
        number_steps = form.number_steps.data
        way = form.way.data

        print(way)
        game.move = [way, number_steps]

    game.step()
    game.counter = game.counter + game.real_num_of_steps

    if game.move[1] !=  game.real_num_of_steps:
        flash(f"You can't go through the  wall ! ")

   # print( f"{game.p_y} , { len(game.world[0] )-1} ")
    if ( game.p_y ==  len(game.world)-1 ) and ( game.p_x ==  len(game.world[0])-1 ):
        flash(f"Game over ! You walked { game.counter} steps.")
        return redirect(url_for('game_over'))

    return render_template("live_box.html", game=game, form=form)

@app.route('/gameover', methods=['GET', 'POST'])
def game_over():
  #  print("creation")

    return render_template("gameover.html")


if __name__=="__main__":
    app.run(host= "0.0.0.0", port =5000)
