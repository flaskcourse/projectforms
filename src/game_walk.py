import random
import copy
from threading import Lock

class SingletonMeta(type):
    _instances = {}
    _lock: Lock = Lock()

    def __call__(cls, *args, **kwargs):
        with cls._lock:
            if cls not in cls._instances or args or kwargs:
                instance = super().__call__(*args, **kwargs)
                cls._instances[cls] = instance
                print("privet")
        return cls._instances[cls]

class GameWalk(metaclass=SingletonMeta):
    def __init__(self, height=20, width=20,  counter=0, system_type="periodic"):
        self.__width = width
        self.__height = height
        s = self.generate_start_position()
        self.world = s[0]
        self.p_x = s[1]
        self.p_y = s[2]
        self.move = [0, 0]
        self.real_num_of_steps = 0

        self.old_world = copy.deepcopy(self.world)
        self.counter = counter
        self.system_type = system_type

    @property
    def width(self):
        return self.__width

    @property
    def height(self):
        return self.__height

    def step(self):
        # self.counter = self.counter+1
        self.old_world = copy.deepcopy(self.world)
        universe = self.world

        new_world = [[0 for _ in range(self.__width)] for _ in range(self.__height)]

       # if self.move[0] == 0:
           # self.p_y = (self.p_y - self.move[1]) % len(universe)
       # elif self.move[0] == 1:
           # self.p_y = (self.p_y + self.move[1]) % len(universe)
       # elif self.move[0] == 2:
           # self.p_x = (self.p_x - self.move[1]) % len(universe[0])
       # elif self.move[0] == 3:
           # self.p_x = (self.p_x + self.move[1]) % len(universe[0])

        if self.move[0] == 0:
            p_y_new = max((self.p_y - self.move[1]), 0)
            self.real_num_of_steps = abs(self.p_y - p_y_new)
            self.p_y = p_y_new

        elif self.move[0] == 1:
            p_y_new = min((self.p_y + self.move[1]), len(universe) - 1)
            self.real_num_of_steps = abs(self.p_y - p_y_new)
            self.p_y = p_y_new

        elif self.move[0] == 2:
            p_x_new = max((self.p_x - self.move[1]), 0)
            self.real_num_of_steps = abs(self.p_x - p_x_new)
            self.p_x = p_x_new

        elif self.move[0] == 3:
            p_x_new = min( (self.p_x + self.move[1]) , len(universe[0])-1)
            self.real_num_of_steps = abs(self.p_x - p_x_new)
            self.p_x = p_x_new

       # px = self.p_x
       # py = self.p_y
        new_world[self.p_y][self.p_x] = 1

        self.world = new_world

    def generate_start_position(self):
        w = self.__width
        h = self.__height
        px = random.randint(0, w-2)
        py = random.randint(0, h-2)

        x = [[0 for _ in range(w)] for _ in range(h)]
        x[ py ][ px ] = 1
        return x, px, py

