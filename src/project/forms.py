from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField, SubmitField, SelectField
from wtforms.validators import NumberRange, InputRequired, DataRequired

class MessageForm(FlaskForm) :
    #height = StringField('height:', validators = [NumberRange(1, 100)])
    #width = StringField('width:', validators = [NumberRange(1, 100)])

    width = IntegerField('Enter width', validators=[
        NumberRange(min=5, max=100, message="width should be from 5 to 100"),
        InputRequired('you did not enter'),
        DataRequired()], default=5)

    height = IntegerField('Enter height', validators=[
        NumberRange(min=5, max=100, message="width should be from 5 to 100"),
        InputRequired('you did not enter'),
        DataRequired()], default=10)

    submit = SubmitField('Submit')

class StepForm(FlaskForm):
    way = SelectField(
        "choose direction",
        coerce=int,
        choices=[
            (0, 'up'),
            (1, 'down'),
            (2, 'left'),
            (3, 'right')
        ],
        render_kw={
            'class': 'form-control'
        },
    )

    number_steps = IntegerField(
        'number of steps ?',
        validators=[NumberRange(min=0,  message="number of steps should be non-negative"), DataRequired()],
        default=1,
        render_kw={
            'class': 'form-control'
        },
    )

    submit = SubmitField('Move')



